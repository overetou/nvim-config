"maps
inoremap ô #include "
inoremap Ô #include <
inoremap ; <End>;
inoremap î if ()<Left>
inoremap ê else<CR>
inoremap û else if ()<Left>
inoremap ŵ while ()<Left>
inoremap â return ;<Left>
inoremap Â static 
inoremap € <C-x><C-o>
nnoremap € :Tagbar<CR>
"autocmd FileType C inoremap . .<C-x><C-o>
inoremap é <Esc>:w<Enter>
inoremap è ->
inoremap à void
inoremap ế \n
inoremap ŷ <Esc>ci[
inoremap ề \0
"Converts a ; statement to an if statement.
nnoremap à Iif (<Esc>f;C)<left>
inoremap ầ <Esc>ci(
set makeprg=ninja\ -C\ build
nnoremap h :wa <bar> pc <bar> make<CR>
"The next line puts the current line of the window below to the top (does a z<CR> on it.
nnoremap <A-é> <C-w>g}<C-w><down>z<CR><C-w><up>
inoremap <A-è> <Esc>ma:exe "normal [("<CR>b<C-w>g}<C-w><down>z<CR><C-w><up>`aa
nnoremap <A-F> :fin 
nnoremap <A-T> :tag 
"This line is not functionnal for now. The goal was to paste something without moving the cursor.
"nnoremap <A-t> ma<down>p<C-o>`a
nnoremap k <C-T>
"The next command make all instances of the symbol under the cursor appear in
"a quickfix window.
nnoremap l :lua vim.lsp.buf.references()<CR>
inoremap ç NULL
"Place func beginning at the top of the screen
nnoremap Z<CR> ma][[[{<down>z<CR>`a
"Retabs the function to 4 tabs.
nnoremap ZZ [[V][:retab! 4<CR>
"Retabs the selection to 4 tabs.
vnoremap ZZ :retab! 4<CR>
"Tabularizes the paragraph. Could really be improved.
nnoremap <C-l> :Tab /\t\S/l0<CR>
"ü (shift-è): build and launch tests with valgrind.
nnoremap é :w<Return>
nnoremap ŝ :vs <bar> te<CR>
inoremap ŝ size_t
inoremap Ŝ sizeof()<left>
"ç (open a buffer in normal mode, insert \n in insert mode, comment selected
"lines in visual mode)
nnoremap ç :b<Space>
vnoremap ç I//<Esc>
vnoremap l :normal 
vnoremap î S)iif<space><esc>f)
vmap { S}%<up>
tnoremap <C-d> <C-\><C-n>:q<CR>
nnoremap <Space> za

inoremap ( ()<left>
"<left><Esc>:exe "normal \<lt>C-w>}\<lt>C-w>\<lt>down>z\<lt>CR>\<lt>C-w>\<lt>up>"<CR>f)i
inoremap [ []<left>
inoremap { {<CR>}<up><end>
inoremap ' ''<left>
inoremap " ""<left>
inoremap /* /* */<left><left>

nnoremap do( ma[(mb%x`bx`a
nnoremap do{ ma[{mb%x`bx`a
nnoremap do[ di[v<left>p
nnoremap do' di'v<left>p
nnoremap do" di"v<left>p
nnoremap do< di<v<left>p

"Remove or change the function the cursor is on.
nnoremap daf ][V%{d
nnoremap caf ][V%{s
"Removes or change the content of the function the cursor is on
nnoremap dif ][di{
nnoremap cif ][ci{

nnoremap j <C-]>ma<up>][[[{<down>z<CR>`a
nnoremap { O{<esc><down>o}<esc><up>==
nnoremap } ma[{mb%dd'bdd=i{`a

"zn and friends
"Measures the length of a string.
nnoremap zn yi":let @"=strlen(@")<CR>
"Add the length of a string after it. Ex: "hello" -> "hello", 5
nnoremap zm yi":let @"=strlen(@")<CR>va"<ESC>a, <ESC>p
"Updates the length of a string in C code.
nnoremap zM yi":let @"=strlen(@")<CR>va"<ESC>wviwp
"Add the length of a string after it and add a ';' at the end of the line.
nnoremap ZM yi":let @"=strlen(@")<CR>va"<ESC>a, <ESC>pA;<ESC>
"Adds the length of a multiline string after it?
inoremap <A-z> <ESC>yi":let @"=strlen(@")<CR>va"<ESC>a, <C-r>"

set omnifunc=v:lua.vim.lsp.omnifunc

"Automatically fold ifdefs, don't create swap files.
set foldmarker=#ifdef,#endif foldmethod=marker noswapfile nobackup
let g:tagbar_sort=0
let g:tagbar_compact=1
let g:tagbar_show_tag_count=1
