set splitright splitbelow mouse=a nowrap rnu nohlsearch noshowmode
set cindent noexpandtab tabstop=4 shiftwidth=4
set completeopt=menu
set numberwidth=2
set sts=0
set comments=://
set path+=**
filetype plugin indent on
"The next line informs us to treat .h files as c files, not cpp.
autocmd BufNewFile,BufRead *.h setlocal filetype=c
autocmd BufNewFile,BufRead *.hpp setlocal filetype=c
autocmd BufNewFile,BufRead *.c setlocal filetype=c
autocmd BufNewFile,BufRead *.cpp setlocal filetype=c
if (has("termguicolors"))
	set termguicolors
endif

function! s:insert_gates()
	let gatename = substitute(toupper(expand("%:t")), "\\.", "_", "g")
	execute "normal! i#ifndef " . gatename
	execute "normal! o# define " . gatename . "\n"
	execute "normal! o#endif"
	execute "normal! kO\n"
endfunction
augroup CFile
	autocmd!
	autocmd BufNewFile *.h call <SID>insert_gates()
augroup END

let g:clang_library_path='/usr/lib64/libclang.so'
"Automatically open quickfix window upon make error.
autocmd QuickFixCmdPost [^l]* nested cwindow
autocmd QuickFixCmdPost    l* nested lwindow

" load the types.vim highlighting file, if it exists
autocmd BufRead,BufNewFile *.[ch] let fname = 'types.vim'
autocmd BufRead,BufNewFile *.[ch] if filereadable(fname)
autocmd BufRead,BufNewFile *.[ch]   exe 'so ' . fname
autocmd BufRead,BufNewFile *.[ch] endif

"Make preview window wrap lines
autocmd BufEnter * if &previewwindow | set wrap linebreak | endif

set previewheight=5 winfixheight

" Plugins will be downloaded under the specified directory.
call plug#begin('~/.vim/plugged')
Plug 'arcticicestudio/nord-vim' "A color theme
Plug 'casperstorm/sort-hvid.vim' "A color theme
Plug 'tpope/vim-surround'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'tpope/vim-fugitive'
Plug 'majutsushi/tagbar'
" LSP Support
Plug 'neovim/nvim-lspconfig'
Plug 'williamboman/mason.nvim', {'do': ':MasonUpdate'}
Plug 'williamboman/mason-lspconfig.nvim'

" Writing
Plug 'junegunn/goyo.vim'
call plug#end()

nnoremap é :w<Return>
nnoremap q :q<Return>
nnoremap Q q
nnoremap <A-q> Q
nnoremap gs :G<Return>4<Down>
nnoremap gl :G log<Return>
nnoremap gw :G push<Return>
nnoremap ç :b<space>
nnoremap <A-w> :Goyo 100<bar> set wrap linebreak noshowmode noshowcmd<CR>
nnoremap gF :e <cfile><CR>
nnoremap <A-a> :cn<CR><C-w><down>z<CR><C-W><up>ma][[[{<down>z<CR>'a
nnoremap <A-b> :cp<CR><C-w><down>z<CR><C-W><up>ma][[[{<down>z<CR>'a
nnoremap <A-m> <C-w><right>:q<CR>
nnoremap <A-ESC> <C-w><down>:q<CR>

"Abrievations
ab ajd aujourd'hui

lua << EOF
require'nvim-treesitter.configs'.setup {
	-- A list of parser names, or "all" (the five listed parsers should always be installed)
	ensure_installed = { "c", "lua", "vim", "vimdoc", "query" },

	-- Install parsers synchronously (only applied to `ensure_installed`)
	sync_install = false,

	-- Automatically install missing parsers when entering buffer
	-- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
	auto_install = true,

	-- List of parsers to ignore installing (for "all")
	ignore_install = { "javascript" },

	---- If you need to change the installation directory of the parsers (see -> Advanced Setup)
	-- parser_install_dir = "/some/path/to/store/parsers", -- Remember to run vim.opt.runtimepath:append("/some/path/to/store/parsers")!

	highlight = {
		enable = true,

		-- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
		-- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
		-- the name of the parser)
		-- list of language that will be disabled
		-- disable = { "c", "rust" },
		-- Or use a function for more flexibility, e.g. to disable slow treesitter highlight for large files
		disable = function(lang, buf)
		local max_filesize = 100 * 1024 -- 100 KB
		local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
		if ok and stats and stats.size > max_filesize then
			return true
			end
			end,

			-- Setting this to true will run `:h syntax` and tree-sitter at the same time.
			-- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
			-- Using this option may slow down your editor, and you may see some duplicate highlights.
			-- Instead of true it can also be a list of languages
			additional_vim_regex_highlighting = false,
			},
}

require('mason').setup()

local lspconfig = require('lspconfig')
require('mason-lspconfig').setup_handlers({
	function(server_name)
		lspconfig[server_name].setup({
			on_attach = lsp_attach,
			capabilities = lsp_capabilities
		})
		end,
})

local groups = {
  all = {
      Folded = {bg = "palette.bg1"}
  }
}

EOF

if strftime("%H") < 18 && strftime("%H") > 7
	colorscheme hvid
else
	colorscheme nord
endif
